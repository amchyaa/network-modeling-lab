import scipy.io as sc
import random
import scipy.stats as st
import networkx as nx

input = sc.loadmat('gnutella.mat')
edges = input["EDGES"].tolist()
G = nx.Graph()
G.add_edges_from(edges)
nodes = G.nodes()
n=len(nodes)

#creating adj list...
adj_list = dict()
for start, end in edges:
	if start not in adj_list.keys() :
		adj_list[start] = [end]
	else :
		adj_list[start].append(end)
	if end not in adj_list.keys() :
		adj_list[end] = [start]
	else :
		adj_list[end].append(start)


#define probabilities
prob = dict()
checks = []
for node in nodes :
	for neighbor in adj_list[node] :
		#P(node,neighbor)=(1/degree(node))*min(1,degree(node)/degree(neighbor))
		if node not in checks :
			prob[node] = [(neighbor,(1/float(G.degree(node)))*min(1,G.degree(node)/float(G.degree(neighbor))))]
			checks.append(node)
		else :
			prob[node].append((neighbor,(1/float(G.degree(node)))*min(1,len(adj_list[node])/float(G.degree(neighbor)))))

# make sure that the sum of the probability = 1
# by giving a probability to stay at a given node
for node in nodes :
	sum_prob =0
	for neighbor, proba in prob[node] :
		sum_prob += proba
	prob[node].append((node,1-sum_prob))

# write the probabilities in way that they can be given 
# to the function st.rv_discrete that generates outputs
# according to their probabilities 
dxs = dict()
dys = dict()
for node in nodes :
	first_time = 1
	for neighbor, proba in prob[node] :
		if first_time :
			dxs[node] = [neighbor]
			dys[node] = [proba]
			first_time = 0
		else :
			dxs[node].append(neighbor)
			dys[node].append(proba)


random_node = random.randint(1,n)
for i in range(400) :
	print G.degree(random_node)
	# a random generator that generate a element from dxs[node] according to
	# its probability in the list of proba dys[node]
	random_gen = st.rv_discrete(values=(dxs[random_node],dys[random_node]))
	old_node = random_node
	random_node = random_gen.rvs()
