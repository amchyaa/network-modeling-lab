import scipy.io as sc
import networkx as nx

# load the input file gnutella.mat
input = sc.loadmat('gnutella.mat')
# convert the set of edges into a python list
edges = input["EDGES"].tolist()
# create an empty graph (python object) using the library networkx
G = nx.Graph()
# add edges to the graph
G.add_edges_from(edges)
# get the set of nodes from the graph
nodes = G.nodes()

degrees = dict()
for node in nodes :
	# get the degree of node "node"
	degrees[node]=G.degree(node)

average =sum(degrees.values())/float(len(nodes))
max = max(degrees.values())
min = min(degrees.values())

#degree distribution
#write to the stdout "node degree_of_node" 
for node in nodes :
	print str(node)+" "+str(degrees[node])