import scipy.io as sc
import networkx as nx
import random

# load the input file gnutella.mat
input = sc.loadmat('gnutella.mat')
# convert the set of edges into a python list
edges = input["EDGES"].tolist()
# create an empty graph (python object) using the library networkx
G = nx.Graph()
# add edges to the graph
G.add_edges_from(edges)
# get the set of nodes from the graph
nodes = G.nodes()

print "the clustering coefficient (def2) "+str(nx.average_clustering(G))
print "the clustering coefficient (def1) "+str(nx.transitivity(G))

#configuration model 
degrees = []

for node in nodes :
	degrees.append(G.degree(node))
	
H = nx.configuration_model(degrees)
#cleaning the configuration model (remove self loops)
H = nx.Graph(H)
H.remove_edges_from(H.selfloop_edges())

print "the clustering coefficient of the configuration model (def2) "+str(nx.average_clustering(H))		
print "the clustering coefficient of the configuration model (def1) "+str(nx.transitivity(H))
	
#We can also compute the clustering coefficient (def1) using this :
"""
adj_list = dict()
for start, end in edges:
	if start not in adj_list.keys() :
		adj_list[start] = [end]
	else :
		adj_list[start].append(end)
	if end not in adj_list.keys() :
		adj_list[end] = [start]
	else :
		adj_list[end].append(start)
		
triangles=0
triples=0
for start, end in edges :
	for node in adj_list[end] :
		if start in adj_list[node] : 
			triangles += 1
			
for node in nodes :
	triples += (len(adj_list[node])*(len(adj_list[node])-1))/2

print "the clustering coefficient (def1) : "+str(triangles/float(triples))
"""