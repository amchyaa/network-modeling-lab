import scipy.io as sc
import networkx as nx

input = sc.loadmat('gnutella.mat')
edges = input["EDGES"].tolist()
G = nx.Graph()
G.add_edges_from(edges)
nodes = G.nodes()

subnet_edges =[]
for start, end in edges :
	if start<=500 and end<=500 :
		subnet_edges.append((start, end))

subnet = nx.Graph()
subnet.add_edges_from(subnet_edges)

print "the diameter of the subnetwork is "+str(nx.diameter(subnet))