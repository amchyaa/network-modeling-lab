import scipy.io as sc
import networkx as nx
import random

# load the input file gnutella.mat
input = sc.loadmat('gnutella.mat')
# convert the set of edges into a python list
edges = input["EDGES"].tolist()
# create an empty graph (python object) using the library networkx
G = nx.Graph()
# add edges to the graph
G.add_edges_from(edges)
# get the set of nodes from the graph
nodes = G.nodes()
n=len(nodes)

#creating adjacency list
adj_list = dict()
for start, end in edges:
	if start not in adj_list.keys() :
		adj_list[start] = [end]
	else :
		adj_list[start].append(end)
	if end not in adj_list.keys() :
		adj_list[end] = [start]
	else :
		adj_list[end].append(start)
		
# RONDOM WALK
for j in range(5) :
	random_node = random.randint(1,n)
	for i in range(400):
		print G.degree(random_node)
		old_list = adj_list[random_node]
		random_node = old_list[random.randint(0,len(old_list)-1)]